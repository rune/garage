//! Crate for handling the admin and metric HTTP APIs
#[macro_use]
extern crate tracing;

pub mod metrics;
pub mod tracing_setup;
